#! /bin/bash
#
# MB 13/02/2019 - Initial tested rhel5,6,7
# Script to get interface IP that has route defined to
# Primary MS server as found in IP AM_PATH/netiq/AM/config/silent.cfg
# Can fix IP if static
# Set Below var if mgmt ip is static and known
#
STATIC_IP=
#========Vars=========
export PATH="$PATH:/usr/sbin:/sbin"
INT_NAME_IGNORE="lo|dock|mast"
INT_COUNT=`ls -A /sys/class/net | egrep -iv $INT_NAME_IGNORE |wc -l`
SOURCE_FILE="/aflac/app//netiq/AM/config/silent.cfg"
MSTR_MS=`grep MS_PRIMARY_NAME $SOURCE_FILE | cut -d = -f 2 | tr -d '"'`

#=======Body==========

# Confirm that MSTR_MS is an IP if not resolve to IP address
if [ `echo $MSTR_MS | grep -c mon` != 0 ]; then
    MSTR_MS_IP=`echo$MSTR_MS`
else
    MSTR_MS_IP=`host $MSTR_MS | awk '/has address/ {print $4;exit}'`
fi

# If two interfaces
if [ $INT_COUNT -ge 2 ]; then
       if [ -z $STATIC_IP ]; then
          MGMT_IP=`ip route get to $MSTR_MS_IP | awk '{print $7}'`
       else
          MGMT_IP=`echo $STATIC_IP`
       fi

# if only one interface - default to hostname
elif [ $INT_COUNT = 1 ]; then
       logger "Appmanager/NetIQ no additional startup options required";
       exit 0

# Unable to define ip revert back to hostname
#elif [ $INT_COUNT > 2 ]; then
#       logger "Netiq/Appmanager Failed to start unable to define mgmt interface refer: $0";
#       exit 1
fi

#=======Output=========

if [ ! -z $MGMT_IP ]; then
   echo "-H $MGMT_IP"
fi
